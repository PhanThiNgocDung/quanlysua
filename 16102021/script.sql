USE [QLSUA]
GO
/****** Object:  UserDefinedFunction [dbo].[Fn_XacDinhMaBac]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_XacDinhMaBac] ( @TichDiem int)
RETURNS CHAR(10)
AS
BEGIN 
	DECLARE @MABAC CHAR(10);
	IF(@TichDiem >=0 AND @TichDiem<100)
	SET @MABAC = 'MOI';
	IF (@TichDiem>= 100 AND @TichDiem< 1000)
	SET @MABAC = 'DONG';
	IF (@TichDiem >= 1000 AND @TichDiem<3000)
	SET @MABAC = 'BAC';
	IF (@TichDiem >=3000 AND @TichDiem <7000)
	SET @MABAC = 'VANG';
	IF (@TichDiem >=7000 AND @TichDiem <12000)
	SET @MABAC = 'BACHKIM';
	IF ( @TichDiem >= 12000) 
	SET @MABAC = 'KIMCUONG';

	RETURN @MABAC;
END;
GO
/****** Object:  Table [dbo].[BAC]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[BAC](
	[MABAC] [char](10) NOT NULL,
	[TENBAC] [nvarchar](50) NULL,
	[DIEMTOITHIEU] [int] NULL,
	[GIAMGIA] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[MABAC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CHITIETPHIEUNHAP]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CHITIETPHIEUNHAP](
	[MACTPN] [char](10) NOT NULL,
	[MAPN] [char](10) NOT NULL,
	[MASUA] [char](10) NOT NULL,
	[NGAYHH] [datetime] NULL,
	[SOLUONG] [int] NULL,
	[DONGIANHAP] [int] NULL,
	[DONGIABAN] [int] NULL,
	[THANHTIEN] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MACTPN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CHITIETPHIEUXUAT]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CHITIETPHIEUXUAT](
	[MACTPX] [char](10) NOT NULL,
	[MAPX] [char](10) NOT NULL,
	[MASUA] [char](10) NOT NULL,
	[SOLUONG] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MACTPX] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CTHOADON]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CTHOADON](
	[MAHD] [char](10) NOT NULL,
	[MASUA] [char](10) NOT NULL,
	[SOLUONG] [int] NULL,
	[DONGIA] [int] NULL,
	[THANHTIEN] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MAHD] ASC,
	[MASUA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HOADON]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HOADON](
	[MAHD] [char](10) NOT NULL,
	[MANV] [char](10) NULL,
	[NGAYLAP] [datetime] NULL,
	[TONGTIEN] [int] NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[HOADON] ADD [MAKH] [char](10) NULL
PRIMARY KEY CLUSTERED 
(
	[MAHD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KHACHHANG]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KHACHHANG](
	[MAKH] [char](10) NOT NULL,
	[HOTEN] [nvarchar](50) NULL,
	[NGAYSINH] [date] NULL,
	[CMND] [char](10) NULL,
	[SDT] [char](10) NULL,
	[TICHDIEM] [int] NULL,
	[MABAC] [char](10) NULL,
 CONSTRAINT [PK__KHACHHAN__603F592C910D2DD3] PRIMARY KEY CLUSTERED 
(
	[MAKH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KHUYENMAI]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[KHUYENMAI](
	[MAKM] [char](10) NOT NULL,
	[TENKM] [nvarchar](50) NULL,
	[GIAMGIA] [float] NULL,
	[NGAYBD] [datetime] NULL,
	[NGAYKT] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MAKM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LichSuGiaBan]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LichSuGiaBan](
	[STT] [int] IDENTITY(1,1) NOT NULL,
	[MaSua] [char](10) NULL,
	[NgayNhap] [datetime] NULL,
	[DonGiaBan] [int] NOT NULL,
 CONSTRAINT [PK_LichSuGiaBan] PRIMARY KEY CLUSTERED 
(
	[STT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOAISUA]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LOAISUA](
	[MALOAI] [int] NOT NULL,
	[TENLOAI] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[MALOAI] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NHACUNGCAP]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHACUNGCAP](
	[MANCC] [char](10) NOT NULL,
	[TENNCC] [nvarchar](50) NULL,
	[SDT] [char](10) NULL,
	[DIACHI] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[MANCC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NHANVIEN]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHANVIEN](
	[MANV] [char](10) NOT NULL,
	[HOVATEN] [nvarchar](50) NULL,
	[GIOITINH] [nvarchar](10) NULL,
	[NAMSINH] [int] NULL,
	[SDT] [nchar](10) NULL,
	[DIACHI] [nvarchar](50) NULL,
	[TAIKHOAN] [char](20) NULL,
	[MATKHAU] [char](20) NULL,
	[CHUCVU] [nvarchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[MANV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NHASANXUAT]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHASANXUAT](
	[MANSX] [char](10) NOT NULL,
	[TENNCC] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[MANSX] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PHIEUNHAP]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PHIEUNHAP](
	[MAPN] [char](10) NOT NULL,
	[MANCC] [char](10) NOT NULL,
	[MANV] [char](10) NOT NULL,
	[TONGTIEN] [int] NULL,
	[NGAYNHAP] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MAPN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PHIEUXUAT]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PHIEUXUAT](
	[MAPX] [char](10) NOT NULL,
	[MANV] [char](10) NOT NULL,
	[NGAYXUAT] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MAPX] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SUA]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SUA](
	[MASUA] [char](10) NOT NULL,
	[TENSUA] [nvarchar](100) NULL,
	[LOAISUA] [int] NULL,
	[MAUSAC] [nvarchar](50) NULL,
	[TRONGLUONG] [int] NULL,
	[THETICH] [int] NULL,
	[HUONGVI] [nvarchar](50) NULL,
	[NSX] [char](10) NULL,
	[SOLUONG] [int] NULL,
	[HINHANH] [char](100) NULL,
	[DonGiaBan] [int] NULL,
	[DOTUOI] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[MASUA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[BAC] ([MABAC], [TENBAC], [DIEMTOITHIEU], [GIAMGIA]) VALUES (N'BAC       ', N'BẠC', 1000, 0.03)
INSERT [dbo].[BAC] ([MABAC], [TENBAC], [DIEMTOITHIEU], [GIAMGIA]) VALUES (N'BACHKIM   ', N'BẠCH KIM', 7000, 0.75)
INSERT [dbo].[BAC] ([MABAC], [TENBAC], [DIEMTOITHIEU], [GIAMGIA]) VALUES (N'DONG      ', N'ÐỒNG', 100, 0.02)
INSERT [dbo].[BAC] ([MABAC], [TENBAC], [DIEMTOITHIEU], [GIAMGIA]) VALUES (N'KIMCUONG  ', N'KIM CUONG', 0, 0.1)
INSERT [dbo].[BAC] ([MABAC], [TENBAC], [DIEMTOITHIEU], [GIAMGIA]) VALUES (N'MOI       ', N'MỚI', 0, 0)
INSERT [dbo].[BAC] ([MABAC], [TENBAC], [DIEMTOITHIEU], [GIAMGIA]) VALUES (N'VANG      ', N'VÀNG', 3000, 0.05)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN01    ', N'PN01      ', N'SB01      ', CAST(0x0000000000000000 AS DateTime), 100, 100000, 20000, 300000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN010   ', N'PN08      ', N'SB02      ', CAST(0x0000ADBA015BF2A8 AS DateTime), 10, 12000, 220000, 130000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN02    ', N'PN01      ', N'SB03      ', CAST(0x0000000000000000 AS DateTime), 50, 150000, 30000, 300000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN03    ', N'PN02      ', N'SB04      ', CAST(0x00008FC100000000 AS DateTime), 100, 200000, 40000, 200000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN04    ', N'PN02      ', N'SB05      ', CAST(0x00008FC100000000 AS DateTime), 1000, 340000, 68000, 400000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN05    ', N'PN02      ', N'SB06      ', CAST(0x00008FC100000000 AS DateTime), 50, 120000, 24000, 400000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN06    ', N'PN03      ', N'SB07      ', CAST(0x0000ADB9017ECED8 AS DateTime), 13, 350000, 40000, 455000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN07    ', N'PN05      ', N'SB08      ', CAST(0x0000ADBA01571944 AS DateTime), 50, 10000, 20000, 500000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN08    ', N'PN06      ', N'SB09      ', CAST(0x0000ADBA0159DEAB AS DateTime), 20, 200000, 30000, 400000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN09    ', N'PN07      ', N'SB18      ', CAST(0x0000ADBA015AA120 AS DateTime), 30, 120000, 30000, 360000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN10    ', N'PN11      ', N'SB11      ', CAST(0x0000ADBA015D86E9 AS DateTime), 6, 12000, 400000, 720000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN11    ', N'PN11      ', N'SB12      ', CAST(0x0000ADBA015D86E9 AS DateTime), 10, 213123, 40000, 127873)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN12    ', N'PN11      ', N'SB13      ', CAST(0x0000ADBA015D86E9 AS DateTime), 100, 213123, 40000, 127873)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN13    ', N'PN11      ', N'SB14      ', CAST(0x0000ADBA015D86E9 AS DateTime), 10, 213123, 40000, 127873)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN14    ', N'PN11      ', N'SB15      ', CAST(0x0000ADBA015D86E9 AS DateTime), 10, 213123, 40000, 127873)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN15    ', N'PN11      ', N'SB16      ', CAST(0x0000ADBA015D86E9 AS DateTime), 10, 213123, 40000, 127873)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN16    ', N'PN11      ', N'SB17      ', CAST(0x0000ADBA015D86E9 AS DateTime), 10, 213123, 40000, 127873)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN17    ', N'PN11      ', N'SB18      ', CAST(0x0000ADBA015D86E9 AS DateTime), 10, 213123, 40000, 127873)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX01    ', N'PX01      ', N'SB01      ', 10)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX02    ', N'PX01      ', N'SB02      ', 5)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX03    ', N'PX02      ', N'SB03      ', 2)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX04    ', N'PX02      ', N'SB04      ', 3)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX05    ', N'PX02      ', N'SB18      ', 4)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX06    ', N'PX04      ', N'SB06      ', 10)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX07    ', N'PX04      ', N'SB07      ', 15)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX08    ', N'PX04      ', N'SB08      ', 2)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX09    ', N'PX04      ', N'SB09      ', 2)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX10    ', N'PX04      ', N'SB10      ', 3)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX11    ', N'PX04      ', N'SB11      ', 3)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX12    ', N'PX04      ', N'SB12      ', 3)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX13    ', N'PX04      ', N'SB13      ', 10)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX14    ', N'PX04      ', N'SB14      ', 3)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX15    ', N'PX04      ', N'SB15      ', 3)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX16    ', N'PX04      ', N'SB16      ', 3)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX17    ', N'PX04      ', N'SB17      ', 3)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX18    ', N'PX04      ', N'SB18      ', 3)
INSERT [dbo].[CTHOADON] ([MAHD], [MASUA], [SOLUONG], [DONGIA], [THANHTIEN]) VALUES (N'HD01      ', N'SB01      ', 1, 529000, 529000)
INSERT [dbo].[CTHOADON] ([MAHD], [MASUA], [SOLUONG], [DONGIA], [THANHTIEN]) VALUES (N'HD02      ', N'SB01      ', 4, 20000, 80000)
INSERT [dbo].[CTHOADON] ([MAHD], [MASUA], [SOLUONG], [DONGIA], [THANHTIEN]) VALUES (N'HD02      ', N'SB02      ', 3, 220000, 660000)
INSERT [dbo].[CTHOADON] ([MAHD], [MASUA], [SOLUONG], [DONGIA], [THANHTIEN]) VALUES (N'HD02      ', N'SB03      ', 6, 30000, 180000)
INSERT [dbo].[CTHOADON] ([MAHD], [MASUA], [SOLUONG], [DONGIA], [THANHTIEN]) VALUES (N'HD03      ', N'SB01      ', 1, 20000, 20000)
INSERT [dbo].[CTHOADON] ([MAHD], [MASUA], [SOLUONG], [DONGIA], [THANHTIEN]) VALUES (N'HD03      ', N'SB02      ', 1, 220000, 220000)
INSERT [dbo].[HOADON] ([MAHD], [MANV], [NGAYLAP], [TONGTIEN], [MAKH]) VALUES (N'HD01      ', N'NV01      ', CAST(0x0000000000000000 AS DateTime), 529000, NULL)
INSERT [dbo].[HOADON] ([MAHD], [MANV], [NGAYLAP], [TONGTIEN], [MAKH]) VALUES (N'HD02      ', N'NV01      ', CAST(0x0000ADC10167CE64 AS DateTime), 920000, N'KH01')
INSERT [dbo].[HOADON] ([MAHD], [MANV], [NGAYLAP], [TONGTIEN], [MAKH]) VALUES (N'HD03      ', N'NV01      ', CAST(0x0000ADC1016EA82B AS DateTime), 240000, NULL)
INSERT [dbo].[KHACHHANG] ([MAKH], [HOTEN], [NGAYSINH], [CMND], [SDT], [TICHDIEM], [MABAC]) VALUES (N'KH01      ', N'Dương Gia Mẫn', CAST(0x0F1F0B00 AS Date), N'225684882 ', N'0816044584', 4112560, N'KIMCUONG  ')
INSERT [dbo].[KHACHHANG] ([MAKH], [HOTEN], [NGAYSINH], [CMND], [SDT], [TICHDIEM], [MABAC]) VALUES (N'KH02      ', N'Nguyễn Thị Lan Anh', CAST(0x47210B00 AS Date), N'227654337 ', N'0935842999', 1450, N'BAC       ')
INSERT [dbo].[KHACHHANG] ([MAKH], [HOTEN], [NGAYSINH], [CMND], [SDT], [TICHDIEM], [MABAC]) VALUES (N'KH03      ', N'Võ Tường Vi', CAST(0x72240B00 AS Date), N'331887222 ', N'0755445722', 50, N'MOI       ')
INSERT [dbo].[KHACHHANG] ([MAKH], [HOTEN], [NGAYSINH], [CMND], [SDT], [TICHDIEM], [MABAC]) VALUES (N'KH04      ', N'Lê Tuấn Anh', CAST(0xDF230B00 AS Date), N'225684884 ', N'0755445721', 7100, N'BACHKIM   ')
INSERT [dbo].[KHUYENMAI] ([MAKM], [TENKM], [GIAMGIA], [NGAYBD], [NGAYKT]) VALUES (N'KM123456  ', N'Mừng sinh nhật 1 năm', 0.05, CAST(0x0000ACB100000000 AS DateTime), CAST(0x0000AF7600000000 AS DateTime))
INSERT [dbo].[LOAISUA] ([MALOAI], [TENLOAI]) VALUES (1, N'Sữa bột')
INSERT [dbo].[LOAISUA] ([MALOAI], [TENLOAI]) VALUES (2, N'Sữa tươi tiệt trùng')
INSERT [dbo].[LOAISUA] ([MALOAI], [TENLOAI]) VALUES (3, N'Sữa hạt, sữa đậu')
INSERT [dbo].[LOAISUA] ([MALOAI], [TENLOAI]) VALUES (4, N'Sữa đặc')
INSERT [dbo].[LOAISUA] ([MALOAI], [TENLOAI]) VALUES (5, N'Sữa chua, phô mai')
INSERT [dbo].[LOAISUA] ([MALOAI], [TENLOAI]) VALUES (6, N'Thức uống lúa mạch')
INSERT [dbo].[LOAISUA] ([MALOAI], [TENLOAI]) VALUES (7, N'Ngũ cốc, ca cao')
INSERT [dbo].[NHACUNGCAP] ([MANCC], [TENNCC], [SDT], [DIACHI]) VALUES (N'NCC01     ', N'Công ty TNHH Nam Phương', N'0935932457', N'29 Nguyễn Sỹ Sách, quận Tân Bình')
INSERT [dbo].[NHACUNGCAP] ([MANCC], [TENNCC], [SDT], [DIACHI]) VALUES (N'NCC02     ', N'Công ty TNHH T&J', N'0935932457', N'29 Nguyễn Sỹ Sách, quận Tân Bình')
INSERT [dbo].[NHACUNGCAP] ([MANCC], [TENNCC], [SDT], [DIACHI]) VALUES (N'NCC03     ', N'Công ty TNHH Hoa Sương', N'0935932457', N'29 Nguyễn Sỹ Sách, quận Tân Bình')
INSERT [dbo].[NHACUNGCAP] ([MANCC], [TENNCC], [SDT], [DIACHI]) VALUES (N'NCC04     ', N'Công ty TNHH Phú Quốc', N'0935932457', N'29 Nguyễn Sỹ Sách, quận Tân Bình')
INSERT [dbo].[NHANVIEN] ([MANV], [HOVATEN], [GIOITINH], [NAMSINH], [SDT], [DIACHI], [TAIKHOAN], [MATKHAU], [CHUCVU]) VALUES (N'NV01      ', N'Nguyễn Thị Mỹ Duyên', N'Nữ', 2000, N'0935831385', N'38 Dương Đức Hiền, quận Tân Phú', N'duyen               ', N'123                 ', N'NV Bán hàng')
INSERT [dbo].[NHANVIEN] ([MANV], [HOVATEN], [GIOITINH], [NAMSINH], [SDT], [DIACHI], [TAIKHOAN], [MATKHAU], [CHUCVU]) VALUES (N'NV02      ', N'Phan Thị Ngọc Dung', N'Nữ', 2000, N'0225445722', N'38 Dương Đức Hiền, quận Tân Phú', N'dung                ', N'123                 ', N'NV kho')
INSERT [dbo].[NHANVIEN] ([MANV], [HOVATEN], [GIOITINH], [NAMSINH], [SDT], [DIACHI], [TAIKHOAN], [MATKHAU], [CHUCVU]) VALUES (N'NV03      ', N'Lý Hoàng Phi Dũng', N'Nam', 2000, N'0935832345', N'38 Dương Đức Hiền, quận Tân Phú', N'phidung             ', N'123                 ', N'Quản lý')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX01     ', N'Vinamilk')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX02     ', N'Dutch Lady')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX04     ', N'Nutifood')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX05     ', N'Nestle')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX06     ', N'Abbott')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX07     ', N'Meadjohnson')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX08     ', N'Friesland Campina')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX09     ', N'Nutimilk')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX10     ', N'Fami')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX11     ', N'Dalat Milk')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX12     ', N'TH')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX13     ', N'LiF')
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN01      ', N'NCC01     ', N'NV01      ', 600000, CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN010     ', N'NCC01     ', N'NV02      ', 0, CAST(0x0000ADBA015CAA95 AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN02      ', N'NCC02     ', N'NV01      ', 1000000, CAST(0x00008FC100000000 AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN03      ', N'NCC03     ', N'NV02      ', 455000, CAST(0x0000ADB9017EC67F AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN04      ', N'NCC02     ', N'NV02      ', 0, CAST(0x0000ADBA0156AE11 AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN05      ', N'NCC01     ', N'NV02      ', 500000, CAST(0x0000ADBA015714CF AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN06      ', N'NCC02     ', N'NV02      ', 400000, CAST(0x0000ADBA0159D8A1 AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN07      ', N'NCC04     ', N'NV02      ', 360000, CAST(0x0000ADBA015A98D1 AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN08      ', N'NCC02     ', N'NV02      ', 130000, CAST(0x0000ADBA015BEB8B AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN09      ', N'NCC01     ', N'NV02      ', 0, CAST(0x0000ADBA015C6E22 AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN11      ', N'NCC01     ', N'NV02      ', 1615111, CAST(0x0000ADBA015D82FD AS DateTime))
INSERT [dbo].[PHIEUXUAT] ([MAPX], [MANV], [NGAYXUAT]) VALUES (N'PX01      ', N'NV01      ', CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[PHIEUXUAT] ([MAPX], [MANV], [NGAYXUAT]) VALUES (N'PX02      ', N'NV01      ', CAST(0x0000ADB700000000 AS DateTime))
INSERT [dbo].[PHIEUXUAT] ([MAPX], [MANV], [NGAYXUAT]) VALUES (N'PX03      ', N'NV02      ', CAST(0x0000ADBA016AAF73 AS DateTime))
INSERT [dbo].[PHIEUXUAT] ([MAPX], [MANV], [NGAYXUAT]) VALUES (N'PX04      ', N'NV02      ', CAST(0x0000ADBA016CFE3F AS DateTime))
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB01      ', N'Sữa Similac số 1', 1, N'Vàng kim', 900, NULL, N'Nguyên chất', N'NSX06     ', 345, N'hinh1.jpg                                                                                           ', 20000, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB02      ', N'Sữa Similac số 2', 1, N'Vàng kim', 900, NULL, N'Vani', N'NSX01     ', 26, N'hinh2.jpg                                                                                           ', 220000, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB03      ', N'Sữa Similac số 3', 1, N'Vàng kim', 900, NULL, N'Nguyên chất', N'NSX06     ', 144, N'hinh3.jpg                                                                                           ', 30000, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB04      ', N'Sữa Similac số 4', 1, N'Vàng kim', 900, NULL, N'Nguyên chất', N'NSX06     ', 300, N'hinh4.jpg                                                                                           ', 40000, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB05      ', N'Sữa Similac mom', 1, N'Vàng kim', 900, NULL, N'Nguyên chất', N'NSX06     ', 3000, N'hinh5.jpg                                                                                           ', 68000, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB06      ', N'Sữa Grow hươu cao cổ 1', 1, N'Trắng xanh lam', 900, NULL, N'Nguyên chất', N'NSX06     ', 150, N'hinh6.jpg                                                                                           ', 24000, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB07      ', N'Sữa Grow hươu cao cổ 2', 1, N'Trắng xanh lá', 900, NULL, N'Nguyên chất', N'NSX06     ', 39, N'hinh7.jpg                                                                                           ', 40000, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB08      ', N'Sữa Grow hươu cao cổ 3', 1, N'Trắng vàng kim', 900, NULL, N'Nguyên chất', N'NSX06     ', 150, N'hinh8.jpg                                                                                           ', 20000, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB09      ', N'Sữa Grow hươu cao cổ 4', 1, N'Trắng vàng kim', 900, NULL, N'Nguyên chất', N'NSX06     ', 60, N'hinh9.jpg                                                                                           ', 30000, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB10      ', N'Sữa Friso Gold 1', 1, N'Xanh lam nhạt', 900, NULL, N'Nguyên chất', N'NSX08     ', 30, N'hinh10.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB11      ', N'Sữa Friso Gold 2', 1, N'Xanh lá nhạt', 900, NULL, N'Nguyên chất', N'NSX08     ', 18, N'hinh11.jpg                                                                                          ', 400000, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB12      ', N'Sữa Friso Gold 3', 1, N'Cam nhạt', 900, NULL, N'Nguyên chất', N'NSX08     ', 30, N'hinh12.jpg                                                                                          ', 40000, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB13      ', N'Sữa Friso Gold 4', 1, N'Tím nhạt', 900, NULL, N'Nguyên chất', N'NSX08     ', 300, N'hinh13.jpg                                                                                          ', 40000, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB14      ', N'Sữa Friso Gold 5', 1, N'Tím nhạt', 900, NULL, N'Nguyên chất', N'NSX08     ', 30, N'hinh14.jpg                                                                                          ', 40000, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB15      ', N'Sữa Friso Gold mom', 1, N'Hồng nhạt', 900, NULL, N'Hương cam', N'NSX08     ', 30, N'hinh15.jpg                                                                                          ', 40000, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB16      ', N'Sữa Dielac Alpha 1', 1, N'Trắng xanh', 900, NULL, N'Nguyên chất', N'NSX01     ', 30, N'hinh16.jpg                                                                                          ', 40000, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB17      ', N'Sữa Dielac Alpha 2', 1, N'Trắng xanh', 900, NULL, N'Nguyên chất', N'NSX01     ', 30, N'hinh17.jpg                                                                                          ', 40000, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB18      ', N'Sữa Dielac Alpha 3', 1, N'Trắng xanh', 900, NULL, N'Nguyên chất', N'NSX01     ', 120, N'hinh18.jpg                                                                                          ', 40000, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB19      ', N'Sữa Dielac Alpha 4', 1, N'Trắng xanh', 900, NULL, N'Nguyên chất', N'NSX01     ', 0, N'hinh19.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SD01      ', N'Sữa đăc Ngôi Sao Phương Nam', 4, N'Trắng và xanh', 380, NULL, N'Nguyên chất', N'NSX01     ', 0, N'hinh34.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SD02      ', N'Sữa đặt có đường Ông Thọ Đỏ', 4, N'Trắng và đỏ', 40, NULL, N'Nguyên chất', N'NSX01     ', 0, N'hinh35.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SD03      ', N'Sữa đặc có đường Dutch Lady', 4, N'Trắng và đỏ', 380, NULL, N'Nguyên chất', N'NSX02     ', 0, N'hinh36.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SDSH01    ', N'Sữa bắp non LiF', 3, N'Trắng và vàng ', NULL, 180, N'Vị bắp', N'NSX13     ', 0, N'hinh29.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SDSH02    ', N'Sữa đậu nành óc chó Vinamilk', 3, N'Trắng và nâu', NULL, 180, N'Đậu nành và óc chó', N'NSX01     ', 0, N'hinh30.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SDSH03    ', N'Sữa đậu nành đậu đỏ Vinamilk', 3, N'Trắng và nâu đỏ', NULL, 180, N'Đậu nành và đậu đỏ', N'NSX01     ', 0, N'hinh31.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SDSH04    ', N'Sữa đậu mè đen Fami ', 3, N'Trắng và vàng', NULL, 200, N'Đậu nành và mè đen', N'NSX10     ', 0, N'hinh32.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SDSH05    ', N'Sữa đậu đỏ đen Fami ', 3, N'Trắng và vàng', NULL, 200, N'Đậu nành và đậu đỏ', N'NSX10     ', 0, N'hinh33.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT01    ', N'Sữa TH True Milk (bịch)', 2, N'Trắng và xanh', NULL, 200, N'Ít đường', N'NSX12     ', 0, N'hinh20.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT02    ', N'Sữa Nestlé NutriStrong', 2, N'Trắng, xanh và hồng', NULL, 180, N'Vị dâu', N'NSX05     ', 0, N'hinh21.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT03    ', N'Sữa TH True Milk (hộp)', 2, N'Trắng, xanh và hồng', NULL, 180, N'Ít đường', N'NSX12     ', 0, N'hinh22.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT04    ', N'Sữa Dutch Lady Canxi & Protein', 2, N'Trắng và xanh', NULL, 220, N'Ít đường', N'NSX02     ', 0, N'hinh23.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT05    ', N'Sữa Nutimilk 100 điểm', 2, N'Trắng và xanh', NULL, 180, N'Ít đường', N'NSX09     ', 0, N'hinh24.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT06    ', N'Sữa Vinamilk Happy Star ', 2, N'Trắng và xanh', NULL, 220, N'Ít đường', N'NSX01     ', 0, N'hinh25.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT07    ', N'Sữa tươi tiệt trùng Dalat Milk', 2, N'Trắng và xanh', NULL, 220, N'Ít đường', N'NSX11     ', 0, N'hinh26.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT08    ', N'Sữa TH True Milk (hộp)', 2, N'Trắng và xanh', NULL, 180, N'Vị socola', N'NSX12     ', 0, N'hinh27.jpg                                                                                          ', 0, N'0-18')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT09    ', N'Sữa TH True Milk (hộp)', 2, N'Trắng và xanh', NULL, 180, N'Vị dâu', N'NSX12     ', 0, N'hinh28.jpg                                                                                          ', 0, N'0-18')
ALTER TABLE [dbo].[CHITIETPHIEUNHAP] ADD  DEFAULT ((0)) FOR [DONGIANHAP]
GO
ALTER TABLE [dbo].[CHITIETPHIEUNHAP] ADD  DEFAULT ((0)) FOR [DONGIABAN]
GO
ALTER TABLE [dbo].[CHITIETPHIEUNHAP]  WITH CHECK ADD  CONSTRAINT [FK_CTPN_PN] FOREIGN KEY([MAPN])
REFERENCES [dbo].[PHIEUNHAP] ([MAPN])
GO
ALTER TABLE [dbo].[CHITIETPHIEUNHAP] CHECK CONSTRAINT [FK_CTPN_PN]
GO
ALTER TABLE [dbo].[CHITIETPHIEUNHAP]  WITH CHECK ADD  CONSTRAINT [FK_CTPN_SUA] FOREIGN KEY([MASUA])
REFERENCES [dbo].[SUA] ([MASUA])
GO
ALTER TABLE [dbo].[CHITIETPHIEUNHAP] CHECK CONSTRAINT [FK_CTPN_SUA]
GO
ALTER TABLE [dbo].[CHITIETPHIEUXUAT]  WITH CHECK ADD  CONSTRAINT [FK_CTPX_PX] FOREIGN KEY([MAPX])
REFERENCES [dbo].[PHIEUXUAT] ([MAPX])
GO
ALTER TABLE [dbo].[CHITIETPHIEUXUAT] CHECK CONSTRAINT [FK_CTPX_PX]
GO
ALTER TABLE [dbo].[CHITIETPHIEUXUAT]  WITH CHECK ADD  CONSTRAINT [FK_CTPX_SUA] FOREIGN KEY([MASUA])
REFERENCES [dbo].[SUA] ([MASUA])
GO
ALTER TABLE [dbo].[CHITIETPHIEUXUAT] CHECK CONSTRAINT [FK_CTPX_SUA]
GO
ALTER TABLE [dbo].[CTHOADON]  WITH CHECK ADD  CONSTRAINT [FK_CTHD_HD] FOREIGN KEY([MAHD])
REFERENCES [dbo].[HOADON] ([MAHD])
GO
ALTER TABLE [dbo].[CTHOADON] CHECK CONSTRAINT [FK_CTHD_HD]
GO
ALTER TABLE [dbo].[CTHOADON]  WITH CHECK ADD  CONSTRAINT [FK_CTHD_SUA] FOREIGN KEY([MASUA])
REFERENCES [dbo].[SUA] ([MASUA])
GO
ALTER TABLE [dbo].[CTHOADON] CHECK CONSTRAINT [FK_CTHD_SUA]
GO
ALTER TABLE [dbo].[HOADON]  WITH CHECK ADD  CONSTRAINT [FK_HD_KH] FOREIGN KEY([MAKH])
REFERENCES [dbo].[KHACHHANG] ([MAKH])
GO
ALTER TABLE [dbo].[HOADON] CHECK CONSTRAINT [FK_HD_KH]
GO
ALTER TABLE [dbo].[HOADON]  WITH CHECK ADD  CONSTRAINT [FK_HD_KHO1] FOREIGN KEY([MANV])
REFERENCES [dbo].[NHANVIEN] ([MANV])
GO
ALTER TABLE [dbo].[HOADON] CHECK CONSTRAINT [FK_HD_KHO1]
GO
ALTER TABLE [dbo].[KHACHHANG]  WITH CHECK ADD  CONSTRAINT [FK_KH_BAC] FOREIGN KEY([MABAC])
REFERENCES [dbo].[BAC] ([MABAC])
GO
ALTER TABLE [dbo].[KHACHHANG] CHECK CONSTRAINT [FK_KH_BAC]
GO
ALTER TABLE [dbo].[PHIEUNHAP]  WITH CHECK ADD  CONSTRAINT [FK_PN_NCC] FOREIGN KEY([MANCC])
REFERENCES [dbo].[NHACUNGCAP] ([MANCC])
GO
ALTER TABLE [dbo].[PHIEUNHAP] CHECK CONSTRAINT [FK_PN_NCC]
GO
ALTER TABLE [dbo].[PHIEUNHAP]  WITH CHECK ADD  CONSTRAINT [FK_PN_NV] FOREIGN KEY([MANV])
REFERENCES [dbo].[NHANVIEN] ([MANV])
GO
ALTER TABLE [dbo].[PHIEUNHAP] CHECK CONSTRAINT [FK_PN_NV]
GO
ALTER TABLE [dbo].[PHIEUXUAT]  WITH CHECK ADD  CONSTRAINT [FK_PX_NV] FOREIGN KEY([MANV])
REFERENCES [dbo].[NHANVIEN] ([MANV])
GO
ALTER TABLE [dbo].[PHIEUXUAT] CHECK CONSTRAINT [FK_PX_NV]
GO
ALTER TABLE [dbo].[SUA]  WITH CHECK ADD  CONSTRAINT [FK_SUA_LOAISUA] FOREIGN KEY([LOAISUA])
REFERENCES [dbo].[LOAISUA] ([MALOAI])
GO
ALTER TABLE [dbo].[SUA] CHECK CONSTRAINT [FK_SUA_LOAISUA]
GO
ALTER TABLE [dbo].[SUA]  WITH CHECK ADD  CONSTRAINT [FK_SUA_NSX] FOREIGN KEY([NSX])
REFERENCES [dbo].[NHASANXUAT] ([MANSX])
GO
ALTER TABLE [dbo].[SUA] CHECK CONSTRAINT [FK_SUA_NSX]
GO
/****** Object:  Trigger [dbo].[capnhatGiaBan]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[capnhatGiaBan]
on [dbo].[CHITIETPHIEUNHAP]
FOR INSERT, UPDATE
AS
BEGIN
	UPDATE SUA
	SET DONGIABAN = inserted.DONGIABAN 
	FROM SUA
	JOIN inserted on  SUA.MASUA = inserted.MASUA
END
GO
/****** Object:  Trigger [dbo].[capnhatSoLuongTon]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[capnhatSoLuongTon]
on [dbo].[CHITIETPHIEUNHAP]
FOR INSERT, UPDATE
AS
BEGIN
	UPDATE SUA
	SET SOLUONG = SUA.SOLUONG + (SELECT SOLUONG FROM inserted WHERE MASUA = SUA.MASUA) 
	FROM SUA
	JOIN inserted on  SUA.MASUA = inserted.MASUA
END
GO
/****** Object:  Trigger [dbo].[capnhatTongTienPN]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[capnhatTongTienPN]
on [dbo].[CHITIETPHIEUNHAP]
FOR INSERT, UPDATE
AS
	UPDATE PHIEUNHAP
	SET TONGTIEN = (SELECT SUM(CHITIETPHIEUNHAP.THANHTIEN) FROM ChiTietPHIEUNHAP WHERE ChiTietPHIEUNHAP.MAPN = (SELECT MAPN FROM inserted))
	WHERE PHIEUNHAP.MAPN = (SELECT MAPN FROM inserted)
GO
/****** Object:  Trigger [dbo].[capnhatSoLuongTonHD]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[capnhatSoLuongTonHD]
on [dbo].[CTHOADON]
FOR INSERT, UPDATE
AS
BEGIN
	UPDATE SUA
	SET SOLUONG = SUA.SOLUONG - (SELECT SOLUONG FROM inserted WHERE MASUA = SUA.MASUA) 
	FROM SUA
	JOIN inserted on  SUA.MASUA = inserted.MASUA
END
GO
/****** Object:  Trigger [dbo].[capnhatTongTien]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[capnhatTongTien]
on [dbo].[CTHOADON]
FOR INSERT, UPDATE
AS
	UPDATE HOADON
	SET TONGTIEN = (SELECT SUM(CTHOADON.THANHTIEN) FROM CTHOADON WHERE CTHOADON.MAHD = (SELECT MAHD FROM inserted))
	WHERE HOADON.MAHD = (SELECT MAHD FROM inserted)

GO
/****** Object:  Trigger [dbo].[capnhatTICHDIEM]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[capnhatTICHDIEM]
on [dbo].[HOADON]
FOR INSERT, UPDATE
AS
BEGIN
	UPDATE KHACHHANG
	SET TICHDIEM = KHACHHANG.TICHDIEM + (SELECT TONGTIEN FROM inserted WHERE MAKH = inserted.MAKH) 
	FROM KHACHHANG
	JOIN inserted on  KHACHHANG.MAKH = inserted.MAKH
END


GO
/****** Object:  Trigger [dbo].[capnhatBAC]    Script Date: 10/16/2021 10:19:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[capnhatBAC]
on [dbo].[KHACHHANG]
FOR INSERT, UPDATE
AS
BEGIN
	UPDATE KHACHHANG
	SET MABAC = (SELECT [dbo].[Fn_XacDinhMaBac](inserted.TICHDIEM) )
	FROM KHACHHANG
	JOIN inserted on  KHACHHANG.MAKH = inserted.MAKH
END

GO
