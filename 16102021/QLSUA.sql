USE [QLSUA]
GO
/****** Object:  Table [dbo].[BAC]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BAC](
	[MABAC] [char](10) NOT NULL,
	[TENBAC] [nvarchar](50) NULL,
	[DIEMTOITHIEU] [int] NULL,
	[GIAMGIA] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[MABAC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CHITIETPHIEUNHAP]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CHITIETPHIEUNHAP](
	[MACTPN] [char](10) NOT NULL,
	[MAPN] [char](10) NOT NULL,
	[MASUA] [char](10) NOT NULL,
	[NGAYHH] [datetime] NULL,
	[SOLUONG] [int] NULL,
	[DONGIANHAP] [int] NULL,
	[DONGIABAN] [int] NULL,
	[THANHTIEN] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MACTPN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CHITIETPHIEUXUAT]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CHITIETPHIEUXUAT](
	[MACTPX] [char](10) NOT NULL,
	[MAPX] [char](10) NOT NULL,
	[MASUA] [char](10) NOT NULL,
	[SOLUONG] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MACTPX] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CTHOADON]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CTHOADON](
	[MAHD] [char](10) NOT NULL,
	[MASUA] [char](10) NOT NULL,
	[SOLUONG] [int] NULL,
	[DONGIA] [int] NULL,
	[THANHTIEN] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MAHD] ASC,
	[MASUA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HOADON]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HOADON](
	[MAHD] [char](10) NOT NULL,
	[MANV] [char](10) NULL,
	[NGAYLAP] [datetime] NULL,
	[TONGTIEN] [int] NULL,
	[MAKH] [char](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[MAHD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KHACHHANG]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KHACHHANG](
	[MAKH] [char](10) NOT NULL,
	[HOTEN] [nvarchar](50) NULL,
	[NGAYSINH] [datetime] NULL,
	[CMND] [char](10) NULL,
	[SDT] [char](10) NULL,
	[TICHDIEM] [int] NULL,
	[MABAC] [char](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[MAKH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KHUYENMAI]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KHUYENMAI](
	[MAKM] [char](10) NOT NULL,
	[TENKM] [nvarchar](50) NULL,
	[GIAMGIA] [float] NULL,
	[NGAYBD] [datetime] NULL,
	[NGAYKT] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MAKM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LichSuGiaBan]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LichSuGiaBan](
	[STT] [int] IDENTITY(1,1) NOT NULL,
	[MaSua] [char](10) NULL,
	[NgayNhap] [datetime] NULL,
	[DonGiaBan] [int] NOT NULL,
 CONSTRAINT [PK_LichSuGiaBan] PRIMARY KEY CLUSTERED 
(
	[STT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOAISUA]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LOAISUA](
	[MALOAI] [int] NOT NULL,
	[TENLOAI] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[MALOAI] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NHACUNGCAP]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHACUNGCAP](
	[MANCC] [char](10) NOT NULL,
	[TENNCC] [nvarchar](50) NULL,
	[SDT] [char](10) NULL,
	[DIACHI] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[MANCC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NHANVIEN]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHANVIEN](
	[MANV] [char](10) NOT NULL,
	[HOVATEN] [nvarchar](50) NULL,
	[GIOITINH] [nvarchar](10) NULL,
	[NAMSINH] [int] NULL,
	[SDT] [nchar](10) NULL,
	[DIACHI] [nvarchar](50) NULL,
	[TAIKHOAN] [char](20) NULL,
	[MATKHAU] [char](20) NULL,
	[CHUCVU] [nvarchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[MANV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NHASANXUAT]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHASANXUAT](
	[MANSX] [char](10) NOT NULL,
	[TENNCC] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[MANSX] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PHIEUNHAP]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PHIEUNHAP](
	[MAPN] [char](10) NOT NULL,
	[MANCC] [char](10) NOT NULL,
	[MANV] [char](10) NOT NULL,
	[TONGTIEN] [int] NULL,
	[NGAYNHAP] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MAPN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PHIEUXUAT]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PHIEUXUAT](
	[MAPX] [char](10) NOT NULL,
	[MANV] [char](10) NOT NULL,
	[NGAYXUAT] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MAPX] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SUA]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SUA](
	[MASUA] [char](10) NOT NULL,
	[TENSUA] [nvarchar](100) NULL,
	[LOAISUA] [int] NULL,
	[MAUSAC] [nvarchar](50) NULL,
	[TRONGLUONG] [int] NULL,
	[THETICH] [int] NULL,
	[HUONGVI] [nvarchar](50) NULL,
	[NSX] [char](10) NULL,
	[SOLUONG] [int] NULL,
	[HINHANH] [char](100) NULL,
	[DonGiaBan] [int] NULL,
	[DOTUOI] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[MASUA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[BAC] ([MABAC], [TENBAC], [DIEMTOITHIEU], [GIAMGIA]) VALUES (N'BAC       ', N'B?C', 1000, 0.03)
INSERT [dbo].[BAC] ([MABAC], [TENBAC], [DIEMTOITHIEU], [GIAMGIA]) VALUES (N'BACHKIM   ', N'B?CH KIM', 7000, 0.75)
INSERT [dbo].[BAC] ([MABAC], [TENBAC], [DIEMTOITHIEU], [GIAMGIA]) VALUES (N'DONG      ', N'Ð?NG', 100, 0.02)
INSERT [dbo].[BAC] ([MABAC], [TENBAC], [DIEMTOITHIEU], [GIAMGIA]) VALUES (N'KIMCUONG  ', N'KIM CUONG', 0, 0.1)
INSERT [dbo].[BAC] ([MABAC], [TENBAC], [DIEMTOITHIEU], [GIAMGIA]) VALUES (N'MOI       ', N'MOI', 0, 0)
INSERT [dbo].[BAC] ([MABAC], [TENBAC], [DIEMTOITHIEU], [GIAMGIA]) VALUES (N'VANG      ', N'VÀNG', 3000, 0.05)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN01    ', N'PN01      ', N'SB01      ', CAST(0x0000000000000000 AS DateTime), 100, 100000, 20000, 300000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN010   ', N'PN08      ', N'SB02      ', CAST(0x0000ADBA015BF2A8 AS DateTime), 10, 12000, 220000, 130000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN02    ', N'PN01      ', N'SB03      ', CAST(0x0000000000000000 AS DateTime), 50, 150000, 30000, 300000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN03    ', N'PN02      ', N'SB04      ', CAST(0x00008FC100000000 AS DateTime), 100, 200000, 40000, 200000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN04    ', N'PN02      ', N'SB05      ', CAST(0x00008FC100000000 AS DateTime), 1000, 340000, 68000, 400000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN05    ', N'PN02      ', N'SB06      ', CAST(0x00008FC100000000 AS DateTime), 50, 120000, 24000, 400000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN06    ', N'PN03      ', N'SB07      ', CAST(0x0000ADB9017ECED8 AS DateTime), 13, 350000, 40000, 455000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN07    ', N'PN05      ', N'SB08      ', CAST(0x0000ADBA01571944 AS DateTime), 50, 10000, 20000, 500000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN08    ', N'PN06      ', N'SB09      ', CAST(0x0000ADBA0159DEAB AS DateTime), 20, 200000, 30000, 400000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN09    ', N'PN07      ', N'SB18      ', CAST(0x0000ADBA015AA120 AS DateTime), 30, 120000, 30000, 360000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN10    ', N'PN11      ', N'SB11      ', CAST(0x0000ADBA015D86E9 AS DateTime), 6, 12000, 400000, 720000)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN11    ', N'PN11      ', N'SB12      ', CAST(0x0000ADBA015D86E9 AS DateTime), 10, 213123, 40000, 127873)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN12    ', N'PN11      ', N'SB13      ', CAST(0x0000ADBA015D86E9 AS DateTime), 100, 213123, 40000, 127873)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN13    ', N'PN11      ', N'SB14      ', CAST(0x0000ADBA015D86E9 AS DateTime), 10, 213123, 40000, 127873)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN14    ', N'PN11      ', N'SB15      ', CAST(0x0000ADBA015D86E9 AS DateTime), 10, 213123, 40000, 127873)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN15    ', N'PN11      ', N'SB16      ', CAST(0x0000ADBA015D86E9 AS DateTime), 10, 213123, 40000, 127873)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN16    ', N'PN11      ', N'SB17      ', CAST(0x0000ADBA015D86E9 AS DateTime), 10, 213123, 40000, 127873)
INSERT [dbo].[CHITIETPHIEUNHAP] ([MACTPN], [MAPN], [MASUA], [NGAYHH], [SOLUONG], [DONGIANHAP], [DONGIABAN], [THANHTIEN]) VALUES (N'CTPN17    ', N'PN11      ', N'SB18      ', CAST(0x0000ADBA015D86E9 AS DateTime), 10, 213123, 40000, 127873)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX01    ', N'PX01      ', N'SB01      ', 10)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX02    ', N'PX01      ', N'SB02      ', 5)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX03    ', N'PX02      ', N'SB03      ', 2)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX04    ', N'PX02      ', N'SB04      ', 3)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX05    ', N'PX02      ', N'SB18      ', 4)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX06    ', N'PX04      ', N'SB06      ', 10)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX07    ', N'PX04      ', N'SB07      ', 15)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX08    ', N'PX04      ', N'SB08      ', 2)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX09    ', N'PX04      ', N'SB09      ', 2)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX10    ', N'PX04      ', N'SB10      ', 3)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX11    ', N'PX04      ', N'SB11      ', 3)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX12    ', N'PX04      ', N'SB12      ', 3)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX13    ', N'PX04      ', N'SB13      ', 10)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX14    ', N'PX04      ', N'SB14      ', 3)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX15    ', N'PX04      ', N'SB15      ', 3)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX16    ', N'PX04      ', N'SB16      ', 3)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX17    ', N'PX04      ', N'SB17      ', 3)
INSERT [dbo].[CHITIETPHIEUXUAT] ([MACTPX], [MAPX], [MASUA], [SOLUONG]) VALUES (N'CTPX18    ', N'PX04      ', N'SB18      ', 3)
INSERT [dbo].[CTHOADON] ([MAHD], [MASUA], [SOLUONG], [DONGIA], [THANHTIEN]) VALUES (N'HD01      ', N'SB01      ', 1, 529000, 529000)
INSERT [dbo].[HOADON] ([MAHD], [MANV], [NGAYLAP], [TONGTIEN], [MAKH]) VALUES (N'HD01      ', N'NV01      ', CAST(0x0000ADBB00000000 AS DateTime), 529000, NULL)
INSERT [dbo].[KHACHHANG] ([MAKH], [HOTEN], [NGAYSINH], [CMND], [SDT], [TICHDIEM], [MABAC]) VALUES (N'KH01      ', N'Dương Gia Mẫn', CAST(0x0000000000000000 AS DateTime), N'225684882 ', N'0935831385', 3560, N'VANG      ')
INSERT [dbo].[KHACHHANG] ([MAKH], [HOTEN], [NGAYSINH], [CMND], [SDT], [TICHDIEM], [MABAC]) VALUES (N'KH02      ', N'Nguyễn Thị Lan Anh', CAST(0x0000000000000000 AS DateTime), N'227654337 ', N'0935842999', 1450, N'BAC       ')
INSERT [dbo].[KHACHHANG] ([MAKH], [HOTEN], [NGAYSINH], [CMND], [SDT], [TICHDIEM], [MABAC]) VALUES (N'KH03      ', N'Võ Tường Vi', CAST(0x0000000000000000 AS DateTime), N'331887222 ', N'0755445722', 50, N'MOI       ')
INSERT [dbo].[KHACHHANG] ([MAKH], [HOTEN], [NGAYSINH], [CMND], [SDT], [TICHDIEM], [MABAC]) VALUES (N'KH04      ', N'Lê Tuấn Khải', CAST(0x0000000000000000 AS DateTime), N'225684884 ', N'0755445721', 7100, N'BACHKIM   ')
INSERT [dbo].[KHACHHANG] ([MAKH], [HOTEN], [NGAYSINH], [CMND], [SDT], [TICHDIEM], [MABAC]) VALUES (N'KH05      ', N'Dương Văn Minh', CAST(0x000093B6017F8158 AS DateTime), N'225684888 ', N'0122544574', 0, N'MOI       ')
INSERT [dbo].[LOAISUA] ([MALOAI], [TENLOAI]) VALUES (1, N'Sữa bột')
INSERT [dbo].[LOAISUA] ([MALOAI], [TENLOAI]) VALUES (2, N'Sữa tươi tiệt trùng')
INSERT [dbo].[LOAISUA] ([MALOAI], [TENLOAI]) VALUES (3, N'Sữa hạt, sữa đậu')
INSERT [dbo].[LOAISUA] ([MALOAI], [TENLOAI]) VALUES (4, N'Sữa đặc')
INSERT [dbo].[LOAISUA] ([MALOAI], [TENLOAI]) VALUES (5, N'Sữa chua, phô mai')
INSERT [dbo].[LOAISUA] ([MALOAI], [TENLOAI]) VALUES (6, N'Thức uống lúa mạch')
INSERT [dbo].[LOAISUA] ([MALOAI], [TENLOAI]) VALUES (7, N'Ngũ cốc, ca cao')
INSERT [dbo].[NHACUNGCAP] ([MANCC], [TENNCC], [SDT], [DIACHI]) VALUES (N'NCC01     ', N'Công ty TNHH Nam Phương', N'0935932457', N'29 Nguyễn Sỹ Sách, quận Tân Bình')
INSERT [dbo].[NHACUNGCAP] ([MANCC], [TENNCC], [SDT], [DIACHI]) VALUES (N'NCC02     ', N'Công ty TNHH T&J', N'0935932457', N'29 Nguyễn Sỹ Sách, quận Tân Bình')
INSERT [dbo].[NHACUNGCAP] ([MANCC], [TENNCC], [SDT], [DIACHI]) VALUES (N'NCC03     ', N'Công ty TNHH Hoa Sương', N'0935932457', N'29 Nguyễn Sỹ Sách, quận Tân Bình')
INSERT [dbo].[NHACUNGCAP] ([MANCC], [TENNCC], [SDT], [DIACHI]) VALUES (N'NCC04     ', N'Công ty TNHH Phú Quốc', N'0935932457', N'29 Nguyễn Sỹ Sách, quận Tân Bình')
INSERT [dbo].[NHANVIEN] ([MANV], [HOVATEN], [GIOITINH], [NAMSINH], [SDT], [DIACHI], [TAIKHOAN], [MATKHAU], [CHUCVU]) VALUES (N'NV01      ', N'Nguyễn Thị Mỹ Duyên', N'Nữ', 2000, N'0935831385', N'38 Dương Đức Hiền, quận Tân Phú', N'duyen               ', N'123                 ', N'NV Bán hàng')
INSERT [dbo].[NHANVIEN] ([MANV], [HOVATEN], [GIOITINH], [NAMSINH], [SDT], [DIACHI], [TAIKHOAN], [MATKHAU], [CHUCVU]) VALUES (N'NV02      ', N'Phan Thị Ngọc Dung', N'Nữ', 2000, N'0225445722', N'38 Dương Đức Hiền, quận Tân Phú', N'dung                ', N'123                 ', N'NV kho')
INSERT [dbo].[NHANVIEN] ([MANV], [HOVATEN], [GIOITINH], [NAMSINH], [SDT], [DIACHI], [TAIKHOAN], [MATKHAU], [CHUCVU]) VALUES (N'NV03      ', N'Lý Hoàng Phi Dũng', N'Nam', 2000, N'0935832345', N'38 Dương Đức Hiền, quận Tân Phú', N'phidung             ', N'123                 ', N'Quản lý')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX01     ', N'Vinamilk')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX02     ', N'Dutch Lady')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX04     ', N'Nutifood')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX05     ', N'Nestle')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX06     ', N'Abbott')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX07     ', N'Meadjohnson')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX08     ', N'Friesland Campina')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX09     ', N'Nutimilk')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX10     ', N'Fami')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX11     ', N'Dalat Milk')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX12     ', N'TH')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX13     ', N'LiF')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX14     ', N'Vinasoy')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX15     ', N'Vegemil')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX16     ', N'YoMost')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX17     ', N'Yumfood')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX18     ', N'CacaoMi')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX19     ', N'Quaker')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX20     ', N'Vietnamcacao')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX21     ', N'VinaCafe')
INSERT [dbo].[NHASANXUAT] ([MANSX], [TENNCC]) VALUES (N'NSX22     ', N'NUTRICARE')
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN01      ', N'NCC01     ', N'NV01      ', 600000, CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN010     ', N'NCC01     ', N'NV02      ', 0, CAST(0x0000ADBA015CAA95 AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN02      ', N'NCC02     ', N'NV01      ', 1000000, CAST(0x00008FC100000000 AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN03      ', N'NCC03     ', N'NV02      ', 455000, CAST(0x0000ADB9017EC67F AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN04      ', N'NCC02     ', N'NV02      ', 0, CAST(0x0000ADBA0156AE11 AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN05      ', N'NCC01     ', N'NV02      ', 500000, CAST(0x0000ADBA015714CF AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN06      ', N'NCC02     ', N'NV02      ', 400000, CAST(0x0000ADBA0159D8A1 AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN07      ', N'NCC04     ', N'NV02      ', 360000, CAST(0x0000ADBA015A98D1 AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN08      ', N'NCC02     ', N'NV02      ', 130000, CAST(0x0000ADBA015BEB8B AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN09      ', N'NCC01     ', N'NV02      ', 0, CAST(0x0000ADBA015C6E22 AS DateTime))
INSERT [dbo].[PHIEUNHAP] ([MAPN], [MANCC], [MANV], [TONGTIEN], [NGAYNHAP]) VALUES (N'PN11      ', N'NCC01     ', N'NV02      ', 1615111, CAST(0x0000ADBA015D82FD AS DateTime))
INSERT [dbo].[PHIEUXUAT] ([MAPX], [MANV], [NGAYXUAT]) VALUES (N'PX01      ', N'NV01      ', CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[PHIEUXUAT] ([MAPX], [MANV], [NGAYXUAT]) VALUES (N'PX02      ', N'NV01      ', CAST(0x0000ADB700000000 AS DateTime))
INSERT [dbo].[PHIEUXUAT] ([MAPX], [MANV], [NGAYXUAT]) VALUES (N'PX03      ', N'NV02      ', CAST(0x0000ADBA016AAF73 AS DateTime))
INSERT [dbo].[PHIEUXUAT] ([MAPX], [MANV], [NGAYXUAT]) VALUES (N'PX04      ', N'NV02      ', CAST(0x0000ADBA016CFE3F AS DateTime))
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC01    ', N'Bột thức uống lúa mạch MIlo', 7, N'Xanh lá', 400, NULL, N'Nguyên chất', N'NSX05     ', 10, N'hinh62.jpg                                                                                          ', 66000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC02    ', N'Ngũ cốc nguyên cám NutiFood', 7, N'Vàng', 500, NULL, N'Nguyên chất', N'NSX04     ', 10, N'hinh63.jpg                                                                                          ', 54800, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC03    ', N'Yến mạch gạo lứt Yumfood', 7, N'Hồng và đỏ', 210, NULL, N'Gạo lứt', N'NSX17     ', 10, N'hinh64.jpg                                                                                          ', 40400, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC04    ', N'Yến mạch nếp cẩm Yumfood', 7, N'Tím', 210, NULL, N'Nếp cẩm', N'NSX17     ', 10, N'hinh65.jpg                                                                                          ', 40400, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC05    ', N'Ngũ cốc dinh dưỡng Yumfood', 7, N'Xanh lá và vàng', 500, NULL, N'Nguyên chất', N'NSX17     ', 10, N'hinh66.jpg                                                                                          ', 56800, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC06    ', N'Ngũ cốc nguyên cám NutiFood', 7, N'Xanh lá', 400, NULL, N'Nguyên chất', N'NSX04     ', 10, N'hinh67.jpg                                                                                          ', 72800, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC07    ', N'Bột cacao nguyên chất CacaoMi', 7, N'Nâu và đỏ', 217, NULL, N'Nguyên chất', N'NSX18     ', 10, N'hinh68.jpg                                                                                          ', 88000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC08    ', N'Bột sữa cacao 3in1 CacaiMi', 7, N'Nâu và xanh lam', 217, NULL, N'Sữa', N'NSX18     ', 10, N'hinh69.jpg                                                                                          ', 67000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC09    ', N'Bột cacao nguyên chất CacaoMi', 7, N'Nâu và xanh lá', 217, NULL, N'Nguyên chất', N'NSX18     ', 10, N'hinh70.jpg                                                                                          ', 66000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC10    ', N'Bột cacao nguyên chất CacaoMi', 7, N'Nâu và cam', 127, NULL, N'Nguyên chất', N'NSX18     ', 10, N'hinh71.jpg                                                                                          ', 57000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC11    ', N'Yến mạch vị chocolate Quaker', 7, N'Nâu và tím', 420, NULL, N'Chocolate', N'NSX19     ', 10, N'hinh72.jpg                                                                                          ', 75000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC12    ', N'Yến mạch truyền thống Quaker', 7, N'Đỏ và kem sữa', 420, NULL, N'Nguyên chất', N'NSX19     ', 10, N'hinh73.jpg                                                                                          ', 75000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC13    ', N'Yến mạch vị dâu berry Quaker', 7, N'Tím', 450, NULL, N'Vị dâu', N'NSX19     ', 10, N'hinh74.jpg                                                                                          ', 75000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC14    ', N'Yến mạch vị trà chanh Quaker', 7, N'Xanh lá ', 336, NULL, N'Vị trà chanh', N'NSX19     ', 10, N'hinh75.jpg                                                                                          ', 75000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC15    ', N'Bột socola Vietnamcacao', 7, N'Nâu', 300, NULL, N'Socola', N'NSX20     ', 10, N'hinh76.jpg                                                                                          ', 94000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC16    ', N'Bột cacao không đường túi', 7, N'Xanh lá', 300, NULL, N'Không đường', N'NSX20     ', 10, N'hinh77.jpg                                                                                          ', 90000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC17    ', N'Bột cacao 5in1 Vietnamcacao', 7, N'Nâu và vàng', 320, NULL, N'Nguyên chất', N'NSX20     ', 10, N'hinh78.jpg                                                                                          ', 55000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC18    ', N'Ngũ cốc Bfast bổ sung canxi', 7, N'Xanh lá và vàng', 500, NULL, N'Nguyên chất', N'NSX21     ', 10, N'hinh79.jpg                                                                                          ', 67000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'NCCC19    ', N'Ngũ cốc VinaCafe Bfast bịch', 7, N'Vàng', 500, NULL, N'Nguyên chất', N'NSX21     ', 10, N'hinh80.jpg                                                                                          ', 67800, NULL)
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB01      ', N'Sữa Similac số 1', 1, N'Vàng kim', 900, NULL, N'Nguyên chất', N'NSX06     ', 350, N'hinh1.jpg                                                                                           ', 529000, N'0 - 6 tháng tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB02      ', N'Sữa Similac số 2', 1, N'Vàng kim', 900, NULL, N'Vani', N'NSX01     ', 30, N'hinh2.jpg                                                                                           ', 545000, N'6 - 12 tháng tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB03      ', N'Sữa Similac số 3', 1, N'Vàng kim', 900, NULL, N'Nguyên chất', N'NSX06     ', 150, N'hinh3.jpg                                                                                           ', 450000, N'1 - 2 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB04      ', N'Sữa Similac số 4', 1, N'Vàng kim', 900, NULL, N'Nguyên chất', N'NSX06     ', 300, N'hinh4.jpg                                                                                           ', 455000, N'2 - 6 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB05      ', N'Sữa Similac mom', 1, N'Vàng kim', 900, NULL, N'Nguyên chất', N'NSX06     ', 3000, N'hinh5.jpg                                                                                           ', 419000, N'Hết thời kỳ mang thai và cho con bú ')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB06      ', N'Sữa Grow hươu cao cổ 1', 1, N'Trắng xanh lam', 900, NULL, N'Nguyên chất', N'NSX06     ', 150, N'hinh6.jpg                                                                                           ', 324000, N'0 - 6 tháng tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB07      ', N'Sữa Grow hươu cao cổ 2', 1, N'Trắng xanh lá', 900, NULL, N'Nguyên chất', N'NSX06     ', 39, N'hinh7.jpg                                                                                           ', 305000, N'6 - 12 tháng tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB08      ', N'Sữa Grow hươu cao cổ 3', 1, N'Trắng vàng kim', 900, NULL, N'Nguyên chất', N'NSX06     ', 150, N'hinh8.jpg                                                                                           ', 275000, N'1 - 2 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB09      ', N'Sữa Grow hươu cao cổ 4', 1, N'Trắng vàng kim', 900, NULL, N'Nguyên chất', N'NSX06     ', 60, N'hinh9.jpg                                                                                           ', 545000, N'2 tuổi trở lên')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB10      ', N'Sữa Friso Gold 1', 1, N'Xanh lam nhạt', 900, NULL, N'Nguyên chất', N'NSX08     ', 30, N'hinh10.jpg                                                                                          ', 533000, N'0 - 6 tháng tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB11      ', N'Sữa Friso Gold 2', 1, N'Xanh lá nhạt', 900, NULL, N'Nguyên chất', N'NSX08     ', 18, N'hinh11.jpg                                                                                          ', 510000, N'6 - 12 tháng tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB12      ', N'Sữa Friso Gold 3', 1, N'Cam nhạt', 900, NULL, N'Nguyên chất', N'NSX08     ', 30, N'hinh12.jpg                                                                                          ', 640000, N'1 - 2 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB13      ', N'Sữa Friso Gold 4', 1, N'Tím nhạt', 900, NULL, N'Nguyên chất', N'NSX08     ', 300, N'hinh13.jpg                                                                                          ', 385000, N'2 - 6 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB14      ', N'Sữa Friso Gold 5', 1, N'Tím nhạt', 900, NULL, N'Nguyên chất', N'NSX08     ', 30, N'hinh14.jpg                                                                                          ', 325000, N'Trên 4 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB15      ', N'Sữa Friso Gold mom', 1, N'Hồng nhạt', 900, NULL, N'Hương cam', N'NSX08     ', 30, N'hinh15.jpg                                                                                          ', 435000, N'Dành cho Mẹ mang thai và cho con bú')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB16      ', N'Sữa Dielac Alpha 1', 1, N'Trắng xanh', 900, NULL, N'Nguyên chất', N'NSX01     ', 30, N'hinh16.jpg                                                                                          ', 210000, N'0 - 6 tháng tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB17      ', N'Sữa Dielac Alpha 2', 1, N'Trắng xanh', 900, NULL, N'Nguyên chất', N'NSX01     ', 30, N'hinh17.jpg                                                                                          ', 228000, N'6 - 12 tháng tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB18      ', N'Sữa Dielac Alpha 3', 1, N'Trắng xanh', 900, NULL, N'Nguyên chất', N'NSX01     ', 120, N'hinh18.jpg                                                                                          ', 185000, N'1 - 2 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB19      ', N'Sữa Dielac Alpha 4', 1, N'Trắng xanh', 900, NULL, N'Nguyên chất', N'NSX01     ', 0, N'hinh19.jpg                                                                                          ', 180000, N'2 - 6 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB20      ', N'Sữa Famna số 1', 1, N'Trắng và xanh lam', 850, NULL, N'Nguyên chất', N'NSX04     ', 0, N'hinh81.jpg                                                                                          ', 469000, N'0 - 6 tháng tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB21      ', N'Sữa Famna số 2', 1, N'Trắng và xanh lam', 850, NULL, N'Nguyên chất', N'NSX04     ', 0, N'hinh82.jpg                                                                                          ', 475000, N'6 - 12 tháng tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB22      ', N'Sữa Famna số 3', 1, N'Trắng và xanh lam', 850, NULL, N'Nguyên chất', N'NSX04     ', 0, N'hinh83.jpg                                                                                          ', 430000, N'1 - 2 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB23      ', N'Sữa Famna số 4', 1, N'Trắng và xanh lam', 850, NULL, N'Nguyên chất', N'NSX04     ', 0, N'hinh84.jpg                                                                                          ', 410000, N'2 tuổi trở lên')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB24      ', N'Sữa NAN Optipro số 1', 1, N'Trắng và xanh lá', 800, NULL, N'Nguyên chất', N'NSX05     ', 0, N'hinh85.jpg                                                                                          ', 380000, N'0 - 6 tháng tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB25      ', N'Sữa NAN Optipro số 2', 1, N'Trắng và xanh lá', 800, NULL, N'Nguyên chất', N'NSX05     ', 0, N'hinh86.jpg                                                                                          ', 395000, N'6 - 12 tháng tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB26      ', N'Sữa NAN Optipro số 3', 1, N'Trắng và xanh lá', 800, NULL, N'Nguyên chất', N'NSX05     ', 0, N'hinh87.jpg                                                                                          ', 360000, N'1 - 2 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB27      ', N'Sữa NAN Optipro số 4', 1, N'Trắng và xanh lá ', 800, NULL, N'Nguyên chất', N'NSX05     ', 0, N'hinh88.jpg                                                                                          ', 380000, N'2 - 6 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB28      ', N'Sữa Nutren Junior Nestlé Thụy Sỹ', 1, N'Trắng và cam', 800, NULL, N'Nguyên chất', N'NSX05     ', 0, N'hinh89.jpg                                                                                          ', 555000, N'1 - 10 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB29      ', N'Sữa Petamin Junior ', 1, N'Trắng và xanh lam', 400, NULL, N'Nguyên chất', N'NSX05     ', 0, N'hinh90.jpg                                                                                          ', 445000, N'1 - 10 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB30      ', N'Sữa BOOST Glucose Control ', 1, N'Trắng và xanh lá', 400, NULL, N'Nguyên chất', N'NSX05     ', 0, N'hinh91.jpg                                                                                          ', 320000, N'Cho người bệnh  tiểu đường')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB31      ', N'Sữa Ensure Gold HMB', 1, N'Trắng và xanh lam', 850, NULL, N'Nguyên chất', N'NSX01     ', 0, N'hinh92.jpg                                                                                          ', 720000, N'Dành cho người lớn')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB32      ', N'Sữa Ensure Đức', 1, N'Trắng và xanh', 400, NULL, N'Nguyên chất', N'NSX01     ', 0, N'hinh93.jpg                                                                                          ', 305000, N'Từ 3 tuổi trở lên')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB33      ', N'Sữa Glucerna ', 1, N'Vàng đồng và hồng', 850, NULL, N'Nguyên chất', N'NSX01     ', 0, N'hinh94.jpg                                                                                          ', 755000, N'Cho người bệnh tiểu đường')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB34      ', N'Sữa Prosure', 1, N'Xanh tím và trắng ', 380, NULL, N'Nguyên chất ', N'NSX01     ', 0, N'hinh95.jpg                                                                                          ', 450000, N'Cho người bệnh ung thư')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB35      ', N'Sữa PediaSure Abbott bổ sung Arginnin và Vitamin K2', 1, N'Tím', 850, NULL, N'Nguyên chất', N'NSX01     ', 0, N'hinh96.jpg                                                                                          ', 575000, N'1 - 10 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB36      ', N'Sữa PediaSure Úc', 1, N'Tím và trắng', 850, NULL, N'Nguyên chất', N'NSX01     ', 0, N'hinh97.jpg                                                                                          ', 750000, N'1 - 10 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB37      ', N'Sữa Meta Care Gold 0+ ', 1, N'Vàng đồng và hồng', 800, NULL, N'Nguyên chất', N'NSX22     ', 0, N'hinh98.jpg                                                                                          ', 450000, N'0 - 12 tháng tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB38      ', N'Sữa Meta Care Gold 0+', 1, N'Vàng đồng và hồng', 400, NULL, N'Nguyên chất', N'NSX22     ', 0, N'hinh99.jpg                                                                                          ', 240000, N'0 - 12 tháng tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SB39      ', N'Sữa Meta Care Gold 1+', 1, N'Vàng đồng và xanh lá', 900, NULL, N'Nguyên chất ', N'NSX22     ', 0, N'hinh100.jpg                                                                                         ', 320000, N'1 - tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SCH01     ', N'Sữa cam YoMost', 5, N'Cam', NULL, 170, N'Vị cam', N'NSX16     ', 10, N'hinh45.jpg                                                                                          ', 29000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SCH02     ', N'Sữa chua SuSu', 5, N'Hồng', NULL, 80, N'Vị dâu', N'NSX01     ', 20, N'hinh46.jpg                                                                                          ', 25500, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SCH03     ', N'Sữa chua việt quất TH', 5, N'Tím và xanh lam', NULL, 180, N'Việt quất', N'NSX12     ', 50, N'hinh47.jpg                                                                                          ', 29000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SCH04     ', N'Sữa chua cam LIF', 5, N'Cam', NULL, 180, N'Vị cam', N'NSX13     ', 50, N'hinh48.jpg                                                                                          ', 27000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SCH05     ', N'Sữa chua yến Nestlé', 5, N'Trắng và xanh lam', NULL, 115, N'Nguyên chất', N'NSX05     ', 30, N'hinh49.jpg                                                                                          ', 26000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SCH06     ', N'Sữa cam Vinamilk Hero', 5, N'Cam', NULL, 110, N'Vị cam', N'NSX01     ', 40, N'hinh50.jpg                                                                                          ', 15500, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SCH07     ', N'Sữa nho Vinamilk Hero', 5, N'Tím', NULL, 110, N'Vị nho', N'NSX01     ', 40, N'hinh51.jpg                                                                                          ', 15500, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SCH08     ', N'Sữa chua cam TH', 5, N'Trắng và cam', NULL, 180, N'Vị cam', N'NSX12     ', 20, N'hinh52.jpg                                                                                          ', 29500, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SCH09     ', N'Sữa chua dâu Vinamilk', 5, N'Trắng và đỏ', NULL, 170, N'Vị dâu', N'NSX01     ', 20, N'hinh53.jpg                                                                                          ', 26000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SCH10     ', N'Sữa chua lựu YoMost ', 5, N'Đỏ và trắng', NULL, 170, N'Vị lựu', N'NSX16     ', 20, N'hinh54.jpg                                                                                          ', 29000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SCH11     ', N'Sữa chua yến mạch Nestlé', 5, N'Trắng và nâu kem', NULL, 180, N'Yến mạch', N'NSX05     ', 35, N'hinh55.jpg                                                                                          ', 15500, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SCH12     ', N'Sữa chua dâu TH', 5, N'Xanh lam và đỏ', NULL, 180, N'Vị dâu', N'NSX12     ', 25, N'hinh56.jpg                                                                                          ', 32000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SD01      ', N'Sữa đăc Ngôi Sao Phương Nam', 4, N'Trắng và xanh', 380, NULL, N'Nguyên chất', N'NSX01     ', 0, N'hinh34.jpg                                                                                          ', 19600, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SD02      ', N'Sữa đặc có đường Ông Thọ Đỏ', 4, N'Trắng và đỏ', 40, NULL, N'Nguyên chất', N'NSX01     ', 0, N'hinh35.jpg                                                                                          ', 5200, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SD03      ', N'Sữa đặc có đường Dutch Lady', 4, N'Trắng và đỏ', 380, NULL, N'Nguyên chất', N'NSX02     ', 0, N'hinh36.jpg                                                                                          ', 23000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SD04      ', N'Sữa đặc Vinamilk Tài Lộc', 4, N'Vàng và đỏ', 380, NULL, N'Nguyên chất', N'NSX01     ', 20, N'hinh37.jpg                                                                                          ', 14000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SD05      ', N'Kem đặc có đường Nuti hộp', 4, N'Xanh lá ', 1284, NULL, N'Nguyên chất', N'NSX04     ', 10, N'hinh38.jpg                                                                                          ', 60000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SD06      ', N'Sữa đặc có đường Dutch Lady Nguyên kem lon', 4, N'Nâu kem', 380, NULL, N'Nguyên chất', N'NSX02     ', 15, N'hinh39.jpg                                                                                          ', 27000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SD07      ', N'Sữa đặc có đường Ông Thọ xanh', 4, N'Xanh và trắng', 380, NULL, N'Nguyên chất', N'NSX01     ', 10, N'hinh40.jpg                                                                                          ', 25000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SDSH01    ', N'Sữa bắp non LiF', 3, N'Trắng và vàng ', NULL, 180, N'Vị bắp', N'NSX13     ', 0, N'hinh29.jpg                                                                                          ', 28000, N'Trẻ lớn hơn 1 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SDSH02    ', N'Sữa đậu nành óc chó Vinamilk', 3, N'Trắng và nâu', NULL, 180, N'Đậu nành và óc chó', N'NSX01     ', 0, N'hinh30.jpg                                                                                          ', 28600, N'người lớn và bé trên 4 tuổi.')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SDSH03    ', N'Sữa đậu nành đậu đỏ Vinamilk', 3, N'Trắng và nâu đỏ', NULL, 180, N'Đậu nành và đậu đỏ', N'NSX01     ', 0, N'hinh31.jpg                                                                                          ', 28600, N'người lớn và bé trên 4 tuổi.')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SDSH04    ', N'Sữa đậu mè đen Fami ', 3, N'Trắng và vàng', NULL, 200, N'Đậu nành và mè đen', N'NSX10     ', 0, N'hinh32.jpg                                                                                          ', 6000, N'Người lớn và trẻ em trên 5 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SDSH05    ', N'Sữa đậu đỏ đen Fami ', 3, N'Trắng và vàng', NULL, 200, N'Đậu nành và đậu đỏ', N'NSX10     ', 0, N'hinh33.jpg                                                                                          ', 6000, N'Người lớn và trẻ em trên 5 tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SDSH06    ', N'Sữa hạt Vegemil', 3, N'Kem sữa ', NULL, 190, N'Nguyên chất', N'NSX15     ', 20, N'hinh41.jpg                                                                                          ', 15700, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SDSH07    ', N'Sữa hạnh nhân óc chó Vegemil', 3, N'Kem sữa', NULL, 190, N'Hạnh nhân, óc chó', N'NSX15     ', 30, N'hinh42.jpg                                                                                          ', 16500, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SDSH08    ', N'Sữa đậu đen và hạt Vegemil', 3, N'Đen và vàng', NULL, 190, N'Đậu đen', N'NSX15     ', 40, N'hinh43.jpg                                                                                          ', 15700, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SDSH09    ', N'Sữa đậu nành Vinasoy', 3, N'Xanh lá và trắng', NULL, 200, N'Đậu nành', N'NSX14     ', 15, N'hinh44.jpg                                                                                          ', 28000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SLM01     ', N'Sữa lúa mạch Milo', 6, N'Xanh lá', NULL, 180, N'Nguyên chất', N'NSX05     ', 20, N'hinh57.jpg                                                                                          ', 27600, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SLM02     ', N'Sữa lúa mạch LIF', 6, N'Xanh lá và nâu', NULL, 180, N'Nguyên chất', N'NSX13     ', 20, N'hinh58.jpg                                                                                          ', 24000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SLM03     ', N'Thức uống dinh dưỡng socola lúa mạch Vinamilk SuSu', 6, N'Xanh lá', NULL, 110, N'Socola và lúa mạch', N'NSX01     ', 20, N'hinh59.jpg                                                                                          ', 5000, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SLM04     ', N'Sữa lon lúa mạch Milo Active Go', 6, N'Xanh lá và nâu', NULL, 240, N'Nguyên chất', N'NSX05     ', 20, N'hinh60.jpg                                                                                          ', 13600, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'SLM05     ', N'Sữa hộp MiloActive Go', 6, N'Xanh lá', NULL, 115, N'Nguyên chất', N'NSX05     ', 23, N'hinh61.jpg                                                                                          ', 19600, N'Mọi lứa tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT01    ', N'Sữa TH True Milk (bịch)', 2, N'Trắng và xanh', NULL, 200, N'Ít đường', N'NSX12     ', 0, N'hinh20.jpg                                                                                          ', 8000, N'Người lớn và trẻ em trên 12 tháng tuổi')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT02    ', N'Sữa Nestlé NutriStrong', 2, N'Trắng, xanh và hồng', NULL, 180, N'Vị dâu', N'NSX05     ', 0, N'hinh21.jpg                                                                                          ', 28000, N'Trẻ từ 1 tuổi trở lên')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT03    ', N'Sữa TH True Milk (hộp)', 2, N'Trắng, xanh và hồng', NULL, 180, N'Ít đường', N'NSX12     ', 0, N'hinh22.jpg                                                                                          ', 31800, N'Trẻ từ 1 tuổi trở lên')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT04    ', N'Sữa Dutch Lady Canxi & Protein', 2, N'Trắng và xanh', NULL, 220, N'Ít đường', N'NSX02     ', 0, N'hinh23.jpg                                                                                          ', 6800, N'Trẻ từ 1 tuổi trở lên')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT05    ', N'Sữa Nutimilk 100 điểm', 2, N'Trắng và xanh', NULL, 180, N'Ít đường', N'NSX09     ', 0, N'hinh24.jpg                                                                                          ', 32000, N'Trẻ từ 1 tuổi trở lên')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT06    ', N'Sữa Vinamilk Happy Star ', 2, N'Trắng và xanh', NULL, 220, N'Ít đường', N'NSX01     ', 0, N'hinh25.jpg                                                                                          ', 6600, N'Trẻ từ 1 tuổi trở lên')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT07    ', N'Sữa tươi tiệt trùng Dalat Milk', 2, N'Trắng và xanh', NULL, 220, N'Ít đường', N'NSX11     ', 0, N'hinh26.jpg                                                                                          ', 34000, N'Trẻ từ 1 tuổi trở lên')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT08    ', N'Sữa TH True Milk (hộp)', 2, N'Trắng và xanh', NULL, 180, N'Vị socola', N'NSX12     ', 0, N'hinh27.jpg                                                                                          ', 34000, N'Trẻ từ 1 tuổi trở lên')
INSERT [dbo].[SUA] ([MASUA], [TENSUA], [LOAISUA], [MAUSAC], [TRONGLUONG], [THETICH], [HUONGVI], [NSX], [SOLUONG], [HINHANH], [DonGiaBan], [DOTUOI]) VALUES (N'STTT09    ', N'Sữa TH True Milk (hộp)', 2, N'Trắng và xanh', NULL, 180, N'Vị dâu', N'NSX12     ', 0, N'hinh28.jpg                                                                                          ', 31500, N'Trẻ từ 1 tuổi trở lên')
GO
ALTER TABLE [dbo].[CHITIETPHIEUNHAP] ADD  DEFAULT ((0)) FOR [DONGIANHAP]
GO
ALTER TABLE [dbo].[CHITIETPHIEUNHAP] ADD  DEFAULT ((0)) FOR [DONGIABAN]
GO
ALTER TABLE [dbo].[CHITIETPHIEUNHAP]  WITH CHECK ADD  CONSTRAINT [FK_CTPN_PN] FOREIGN KEY([MAPN])
REFERENCES [dbo].[PHIEUNHAP] ([MAPN])
GO
ALTER TABLE [dbo].[CHITIETPHIEUNHAP] CHECK CONSTRAINT [FK_CTPN_PN]
GO
ALTER TABLE [dbo].[CHITIETPHIEUNHAP]  WITH CHECK ADD  CONSTRAINT [FK_CTPN_SUA] FOREIGN KEY([MASUA])
REFERENCES [dbo].[SUA] ([MASUA])
GO
ALTER TABLE [dbo].[CHITIETPHIEUNHAP] CHECK CONSTRAINT [FK_CTPN_SUA]
GO
ALTER TABLE [dbo].[CHITIETPHIEUXUAT]  WITH CHECK ADD  CONSTRAINT [FK_CTPX_PX] FOREIGN KEY([MAPX])
REFERENCES [dbo].[PHIEUXUAT] ([MAPX])
GO
ALTER TABLE [dbo].[CHITIETPHIEUXUAT] CHECK CONSTRAINT [FK_CTPX_PX]
GO
ALTER TABLE [dbo].[CHITIETPHIEUXUAT]  WITH CHECK ADD  CONSTRAINT [FK_CTPX_SUA] FOREIGN KEY([MASUA])
REFERENCES [dbo].[SUA] ([MASUA])
GO
ALTER TABLE [dbo].[CHITIETPHIEUXUAT] CHECK CONSTRAINT [FK_CTPX_SUA]
GO
ALTER TABLE [dbo].[CTHOADON]  WITH CHECK ADD  CONSTRAINT [FK_CTHD_HD] FOREIGN KEY([MAHD])
REFERENCES [dbo].[HOADON] ([MAHD])
GO
ALTER TABLE [dbo].[CTHOADON] CHECK CONSTRAINT [FK_CTHD_HD]
GO
ALTER TABLE [dbo].[CTHOADON]  WITH CHECK ADD  CONSTRAINT [FK_CTHD_SUA] FOREIGN KEY([MASUA])
REFERENCES [dbo].[SUA] ([MASUA])
GO
ALTER TABLE [dbo].[CTHOADON] CHECK CONSTRAINT [FK_CTHD_SUA]
GO
ALTER TABLE [dbo].[HOADON]  WITH CHECK ADD  CONSTRAINT [FK_HD_KH] FOREIGN KEY([MAKH])
REFERENCES [dbo].[KHACHHANG] ([MAKH])
GO
ALTER TABLE [dbo].[HOADON] CHECK CONSTRAINT [FK_HD_KH]
GO
ALTER TABLE [dbo].[HOADON]  WITH CHECK ADD  CONSTRAINT [FK_HD_KHO1] FOREIGN KEY([MANV])
REFERENCES [dbo].[NHANVIEN] ([MANV])
GO
ALTER TABLE [dbo].[HOADON] CHECK CONSTRAINT [FK_HD_KHO1]
GO
ALTER TABLE [dbo].[KHACHHANG]  WITH CHECK ADD  CONSTRAINT [FK_KH_BAC] FOREIGN KEY([MABAC])
REFERENCES [dbo].[BAC] ([MABAC])
GO
ALTER TABLE [dbo].[KHACHHANG] CHECK CONSTRAINT [FK_KH_BAC]
GO
ALTER TABLE [dbo].[PHIEUNHAP]  WITH CHECK ADD  CONSTRAINT [FK_PN_NCC] FOREIGN KEY([MANCC])
REFERENCES [dbo].[NHACUNGCAP] ([MANCC])
GO
ALTER TABLE [dbo].[PHIEUNHAP] CHECK CONSTRAINT [FK_PN_NCC]
GO
ALTER TABLE [dbo].[PHIEUNHAP]  WITH CHECK ADD  CONSTRAINT [FK_PN_NV] FOREIGN KEY([MANV])
REFERENCES [dbo].[NHANVIEN] ([MANV])
GO
ALTER TABLE [dbo].[PHIEUNHAP] CHECK CONSTRAINT [FK_PN_NV]
GO
ALTER TABLE [dbo].[PHIEUXUAT]  WITH CHECK ADD  CONSTRAINT [FK_PX_NV] FOREIGN KEY([MANV])
REFERENCES [dbo].[NHANVIEN] ([MANV])
GO
ALTER TABLE [dbo].[PHIEUXUAT] CHECK CONSTRAINT [FK_PX_NV]
GO
ALTER TABLE [dbo].[SUA]  WITH CHECK ADD  CONSTRAINT [FK_SUA_LOAISUA] FOREIGN KEY([LOAISUA])
REFERENCES [dbo].[LOAISUA] ([MALOAI])
GO
ALTER TABLE [dbo].[SUA] CHECK CONSTRAINT [FK_SUA_LOAISUA]
GO
ALTER TABLE [dbo].[SUA]  WITH CHECK ADD  CONSTRAINT [FK_SUA_NSX] FOREIGN KEY([NSX])
REFERENCES [dbo].[NHASANXUAT] ([MANSX])
GO
ALTER TABLE [dbo].[SUA] CHECK CONSTRAINT [FK_SUA_NSX]
GO
/****** Object:  Trigger [dbo].[capnhatGiaBan]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[capnhatGiaBan]
on [dbo].[CHITIETPHIEUNHAP]
FOR INSERT, UPDATE
AS
BEGIN
	UPDATE SUA
	SET DONGIABAN = inserted.DONGIABAN 
	FROM SUA
	JOIN inserted on  SUA.MASUA = inserted.MASUA
END


GO
/****** Object:  Trigger [dbo].[capnhatSoLuongTon]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[capnhatSoLuongTon]
on [dbo].[CHITIETPHIEUNHAP]
FOR INSERT, UPDATE
AS
BEGIN
	UPDATE SUA
	SET SOLUONG = SUA.SOLUONG + (SELECT SOLUONG FROM inserted WHERE MASUA = SUA.MASUA) 
	FROM SUA
	JOIN inserted on  SUA.MASUA = inserted.MASUA
END


GO
/****** Object:  Trigger [dbo].[capnhatTongTienPN]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[capnhatTongTienPN]
on [dbo].[CHITIETPHIEUNHAP]
FOR INSERT, UPDATE
AS
	UPDATE PHIEUNHAP
	SET TONGTIEN = (SELECT SUM(CHITIETPHIEUNHAP.THANHTIEN) FROM ChiTietPHIEUNHAP WHERE ChiTietPHIEUNHAP.MAPN = (SELECT MAPN FROM inserted))
	WHERE PHIEUNHAP.MAPN = (SELECT MAPN FROM inserted)


GO
/****** Object:  Trigger [dbo].[capnhatSoLuongTonHD]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[capnhatSoLuongTonHD]
on [dbo].[CTHOADON]
FOR INSERT, UPDATE
AS
BEGIN
	UPDATE SUA
	SET SOLUONG = SUA.SOLUONG - (SELECT SOLUONG FROM inserted WHERE MASUA = SUA.MASUA) 
	FROM SUA
	JOIN inserted on  SUA.MASUA = inserted.MASUA
END


GO
/****** Object:  Trigger [dbo].[capnhatTongTien]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[capnhatTongTien]
on [dbo].[CTHOADON]
FOR INSERT, UPDATE
AS
	UPDATE HOADON
	SET TONGTIEN = (SELECT SUM(CTHOADON.THANHTIEN) FROM CTHOADON WHERE CTHOADON.MAHD = (SELECT MAHD FROM inserted))
	WHERE HOADON.MAHD = (SELECT MAHD FROM inserted)


GO
/****** Object:  Trigger [dbo].[capnhatTICHDIEM]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[capnhatTICHDIEM]
on [dbo].[HOADON]
FOR INSERT, UPDATE
AS
BEGIN
	UPDATE KHACHHANG
	SET TICHDIEM = KHACHHANG.TICHDIEM + (SELECT TONGTIEN FROM inserted WHERE MAKH = inserted.MAKH) 
	FROM KHACHHANG
	JOIN inserted on  KHACHHANG.MAKH = inserted.MAKH
END
GO
/****** Object:  Trigger [dbo].[capnhatBAC]    Script Date: 10/30/2021 8:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[capnhatBAC]
on [dbo].[KHACHHANG]
FOR INSERT, UPDATE
AS
BEGIN
	UPDATE KHACHHANG
	SET MABAC = (SELECT [dbo].[Fn_XacDinhMaBac](inserted.TICHDIEM) )
	FROM KHACHHANG
	JOIN inserted on  KHACHHANG.MAKH = inserted.MAKH
END
GO
